import http from 'http';
import { createReadStream, readFile } from 'fs';
import path from 'path';

http.createServer(function (request, response) {
    console.log('request starting...');

    let filePath = '.' + request.url;

    let extname = path.extname(filePath);
    let contentType = '';
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.json':
            contentType = 'application/json';
            break;
        case '.png':
            filePath = './dist/assets' + request.url;
            contentType = 'image/png';
            break;
        case '.jpg':
            filePath = './dist/assets' + request.url;
            contentType = 'image/jpg';
            break;
        case '.wav':
            contentType = 'audio/wav';
            break;
        case '.html':
            filePath = './dist/pages' + request.url;
            contentType = 'text/html';
            break;
        case '.txt':
            filePath = './dist/pages' + request.url;
            contentType = 'text/plain';
            break;
    }

    if (filePath == './') {
        filePath = './dist/pages/index.html';
    }


    readFile(filePath, (error, content) => {
        if (error) {
            if (error.code == 'ENOENT') {
                readFile('./dist/pages/notFound.html', (error, content) => {
                    response.writeHead(200, { 'Content-Type': contentType });
                    response.end(content, 'utf-8');
                });
            }
            else {
                response.writeHead(500);
                response.end('Sorry, check with the site admin for error: ' + error.code + ' ..\n');
                response.end();
            }
        }
        else {
            response.writeHead(200, { 'Content-Type': contentType });
            response.end(content, 'utf-8');
        }
    });

}).listen(8080);
console.log('Server running at http://localhost:8080/');
