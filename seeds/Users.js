let faker = require('faker');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('User').del()
    .then(function () {
      // Inserts seed entries
      return knex('User').insert(generirajUporabnike(20));
    });
};


function generirajUporabnike(st){
  let poljeUporabnikov= [];
  for(let i = 0;i<st;i++){
    poljeUporabnikov.push({
      id: i,
      name: faker.name.firstName(),
      lastname: faker.name.lastName(),
      username: faker.random.word(),
      password: faker.random.word(),
      birthdate: faker.date.between('1960-01-01', '2005-01-05'),
      address: faker.address.streetAddress(),
      telephone: faker.phone.phoneNumber(),
      picture: faker.image.people()
    })
}
return poljeUporabnikov;
}