let faker = require('faker');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('City').del()
    .then(function () {
      // Inserts seed entries
      return knex('City').insert(generirajMesta(20));
    });
};


function generirajMesta(st){
  let poljeMest= [];
  for(let i = 0;i<st;i++){
    poljeMest.push({
      id: i,
      address: faker.address.city()
    })
}
return poljeMest;
}