let faker = require('faker');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('Job').del()
    .then(function () {
      // Inserts seed entries
      return knex('Job').insert(generirajDela(20));
    });
};


function generirajDela(st){
  let poljeDel= [];
  for(let i = 0;i<st;i++){
    poljeDel.push({
      id: i,
      job_type: faker.name.jobType(),
      Hourly_rate: faker.random.number({min:5, max: 10}),
      expiration: faker.date.future(),
      description: faker.name.jobDescriptor(),
      City_id:i
    })
}
return poljeDel;
}