var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
const fetch = require("node-fetch");
const knex = require('knex')(require('../knexfile').development);
const bookshelf = require('bookshelf')(knex);

//let Uporabniki = new Array();
//let Dela = new Array();




const Uporabniki = bookshelf.Model.extend({
  tableName: 'User',
  idAttribute: 'id',
  username: 'username',
  password: 'password'
});

const Mesta = bookshelf.Model.extend({
  tableName: 'City',
  idAttribute: 'id'
});

const Dela = bookshelf.Model.extend({
  tableName: 'Job',
  idAttribute: 'id',
  mesto: function(){
    return this.belongsTo(Mesta)
  }
});

router.get('/',  async function( req, res) {
  try{
   const upora= await new Uporabniki().fetchAll();
   res.json(upora.toJSON())
  }catch(err){
    res.json(err)
  }
  
});


router.post('/registracija',  async function( req, res) {
    try{
      
      const {name,lastname,username,password,birthdate,address,telephone,picture} = req.body;
      const nov = await new Uporabniki().save({name,lastname,username,password,birthdate,address,telephone,picture})
      res.json("Registracija uspešna");
    }catch(err){
      res.json(err)
    }
    
});

router.get('/prijava/:uporabnisko_ime/:geslo', async function( req, res) {
var geslo;
try{
  const x = await new Uporabniki({'username': req.params.uporabnisko_ime})
  .fetch()
  .then(function(model) {
     geslo= (model.get('password'));
  });

  if (geslo === req.params.geslo) { 
    res.json("Prijava uspešna")
  }else{
    res.json("Prijava ne uspešna")
  }
}catch(err){
  res.json(err)
}

});

router.post('/delo',  async function( req, res) {
  try{

    const {job_type,Hourly_rate,expiration,description,City_id} = req.body;
      const nov = await new Dela().save({job_type,Hourly_rate,expiration,description,City_id})
    res.json("Delo uspešno dodano")
  }catch(err){
    res.json(err)
  }
  
});

router.get('/dela',  async function( req, res) {
  try{
    const delo= await new Dela().fetchAll({withRelated: ['mesto']});
   res.json(delo.toJSON())
  }catch(err){
    res.json(err)
  }
  
});

router.get('/delo/:idDela',  async function( req, res) {
try{
  const x = await new Dela({'id': req.params.idDela})
  .fetch({withRelated: ['mesto']});
    res.json(x)
}catch(err){
  res.json(err)
}

});

router.delete('/delo/:idDela', async function( req, res) {
  try{
    new Dela({'id': req.params.idDela}).destroy();
      res.json("Delo izbrisano")
  }catch(err){
    res.json(err)
  }
});
module.exports = router;
