
exports.up = function(knex) {
    return knex.schema
    .createTable('User',(table) => {
     table.increments('id').primary();
     table.string('name');
     table.string("lastname");
     table.string("username");
     table.string("password");
     table.string("birthdate");
     table.string("address");
     table.integer("telephone");
     table.string("picture")

     })
     .createTable('City',(table) => {
        table.increments('id').primary();
        table.string('address');
        })
    .createTable('Job',(table) => {
     table.increments('id').primary();
     table.string('job_type')
     table.string("Hourly_rate")
     table.string("expiration");
     table.integer("description");
     table.integer("City_id")
    .unique().references('id').inTable('City');
     })
     .createTable('Publishing',(table) => {
        table.increments('id').primary();
        table.string('date');
        table.string("publish_type");
        table.integer("id_job").unique().references('id').inTable('Job');
        table.integer("id_user").unique().references('id').inTable('User');
        });
};

exports.down = function(knex) {
    return knex.schema.dropTable('User').dropTable('City').dropTable("Job").dropTable("Publishing");
};
